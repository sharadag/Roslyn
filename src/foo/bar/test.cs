using Microsoft.VisualStudio.Acquisition.Model;
namespace Microsoft.VisualStudio.Acquisition.Agents.Bundle
{
    public static class PackageCache
    {
        public static string RootDirectory
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Package Cache");
            }
        }
        public static bool Contains(IPackage bundle)
        {
            var path = PackageCache.Resolve(bundle);
            return File.Exists(path);
        }
        public static string Resolve(IPackage bundle)
        {
            if (null == bundle)
            {
                throw new ArgumentNullException("bundle");
            }

            var path = Path.Combine(PackageCache.RootDirectory, bundle.Name);
            var name = Path.GetFileName(bundle.Path);

            return Path.Combine(path, name);
        }
    }
}
